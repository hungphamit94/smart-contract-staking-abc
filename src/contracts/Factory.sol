// SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/utils/math/Math.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/utils/Context.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Address.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "hardhat/console.sol";

abstract contract IRewardDistributionRecipient is Ownable {
    address rewardDistribution;

    function notifyRewardAmount(uint256 reward) external virtual;

    modifier onlyRewardDistribution() {
        require(_msgSender() == rewardDistribution, "Caller is not reward distribution");
        _;
    }

    function setRewardDistribution(address _rewardDistribution)
        external
        onlyOwner
    {
        rewardDistribution = _rewardDistribution;
    }
}

// File: contracts/CurveRewards.sol

pragma solidity ^0.8.2;

abstract contract StakeTokenWrapper {
    using SafeMath for uint256;
    using SafeERC20 for IERC20;

    IERC20 public stakeToken;

    uint256 private _totalSupply;
    mapping(address => uint256) private _balances;

    constructor(address _stakeTokenAddress)  {
         stakeToken = IERC20(_stakeTokenAddress);
    }

    function totalSupply() public view returns (uint256) {
        return _totalSupply;
    }

    function balanceOf(address account) public view returns (uint256) {
        return _balances[account];
    }

    function stake(uint256 amount) public virtual  {
        stakeToken.approve(msg.sender, amount);
        stakeToken.approve(address(this), amount);
        _totalSupply = _totalSupply.add(amount);
        _balances[msg.sender] = _balances[msg.sender].add(amount);
        stakeToken.safeTransferFrom(msg.sender, address(this), amount);
        
    }

    function withdraw(uint256 amount) public virtual {
        _totalSupply = _totalSupply.sub(amount);
        _balances[msg.sender] = _balances[msg.sender].sub(amount);
        stakeToken.transfer(msg.sender, amount);
    }
}

contract RenaStakingRewards is StakeTokenWrapper, IRewardDistributionRecipient {
    using SafeERC20 for IERC20;
    using SafeMath for uint256;
    IERC20 public rewardToken; 
    IERC20 public exampleToken;
    uint256 public apy;
    uint256 public duration;
    uint256 public constant LOCK_PERIOD = 5 minutes; // LOCK_PERIOD <= DURATION

    uint256 public periodFinish = 0;
    uint256 public rewardRate = 0;
    uint256 public lastUpdateTime;
    uint256 public rewardPerTokenStored;
    uint256 public startTime;
    uint256 public limitedStake;

    mapping(address => uint256) public totalStake;
    mapping(address => uint256) public userRewardPerTokenPaid;
    mapping(address => uint256) public rewards;
    mapping(address => uint256) public withdrawTime;

    event RewardAdded(uint256 reward);
    event Staked(address indexed user, uint256 amount);
    event Withdrawn(address indexed user, uint256 amount);
    event RewardPaid(address indexed user, uint256 reward);

    constructor(address _rewardTokenAddress, address _stakeTokenAddress, uint256 _apy, uint256 _limitedStake) StakeTokenWrapper(_stakeTokenAddress) {
        rewardToken = IERC20(_rewardTokenAddress);
        apy = _apy;
        limitedStake = _limitedStake;
    }

    function earned(address account) public view returns (uint256) {
        uint date = (block.timestamp - startTime) / 60 / 60 / 24;
        return
            totalStake[account]
                .mul(date)
                .div(365)
                .mul(apy)
                .mul(1e18);

    }

    // stake visibility is public as overriding StakeTokenWrapper's stake() function
    function stake(uint256 amount) public override  {
        require(amount > 0, "Cannot stake 0");
        super.stake(amount);
        startTime = block.timestamp;
        uint256 canWithdrawTime  = block.timestamp.add(LOCK_PERIOD);
        if (canWithdrawTime > periodFinish) {
            canWithdrawTime = periodFinish;
        } 
        withdrawTime[msg.sender] = canWithdrawTime;
        emit Staked(msg.sender, amount);
    }

    function withdraw(uint256 amount) public override {
        require(amount > 0, "Cannot withdraw 0");
        require(block.timestamp >= withdrawTime[msg.sender], "Account still in lock");
        super.withdraw(amount);  // no need to update withdrawTime here
        emit Withdrawn(msg.sender, amount);
    }

    function exit() external {
        withdraw(balanceOf(msg.sender));
        getReward();
    }

    function getReward() public {
        uint256 reward = earned(msg.sender);
        if (reward > 0) {
            rewards[msg.sender] = 0;
            rewardToken.safeTransfer(msg.sender, reward);
            emit RewardPaid(msg.sender, reward);
        }
    }

    function notifyRewardAmount(uint256 reward)
        external
        onlyRewardDistribution
        override 
    {
        if (block.timestamp >= periodFinish) {
            rewardRate = reward.div(duration);
        } else {
            uint256 remaining = periodFinish.sub(block.timestamp);
            uint256 leftover = remaining.mul(rewardRate);
            rewardRate = reward.add(leftover).div(duration);
        }
        lastUpdateTime = block.timestamp;
        periodFinish = block.timestamp.add(duration);
        emit RewardAdded(reward);
    }
}

pragma solidity ^0.8.2;

contract Factory{
     RenaStakingRewards[] public renaStakingRewards;
    //  uint disabledCount;
    address public owner;
    
    constructor() public {
        owner = msg.sender;
    }
    event renaStakingRewardCreated(address renaStakingRewardAddress, address _rewardTokenAddress, address _stakeTokenAddress, uint _apy);

    function createRenaStakingRewardContract(address _rewardTokenAddress, address _stakeTokenAddress, uint _apy, uint _limitedStake) external{
       RenaStakingRewards staking = new RenaStakingRewards(_rewardTokenAddress, _stakeTokenAddress, _apy, _limitedStake);
       renaStakingRewards.push(staking);
       emit renaStakingRewardCreated(address(staking), _rewardTokenAddress, _stakeTokenAddress, _apy);
    }

    function getRenaStakingRewards() external view returns(RenaStakingRewards[] memory _renaStakingRewards){
        return renaStakingRewards;
    }  
     
    function renaImplementStaking(uint _id, uint _amount) public {
         RenaStakingRewards renaStaking = renaStakingRewards[_id];
         renaStaking.stake(_amount);
    }
 
}