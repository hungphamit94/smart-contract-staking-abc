import React, { Component } from 'react'
import dai from '../dai.png'

class Main extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      rewardAddress: '',
      stakingAddress: '',
      APY: '',
      limited: '',
      listStakingContract: []
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
      const listStakingTokenContract = await this.props.factory.methods.getRenaStakingRewards().call()
      this.setState({listStakingContract: listStakingTokenContract})
  }

  handleChange(event) {
    const value = event.target.value;
    const name = event.target.name;
    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.createStakingContract(this.state.rewardAddress, this.state.stakingAddress, this.state.APY, this.state.limited);
  }

  render() {
    var listStaking = '';
      listStaking = this.state.listStakingContract.map((address, i) => {                      
      // Return the element. Also pass key     
      return (
          <tr>
          <td>{i}</td>
          <td>{address}</td>
          </tr>
      ) 
    })
    return (
      <div id="content" className="mt-3">

        <table className="table table-borderless text-muted text-center">
          <thead>
            <tr>
              <th scope="col">Id</th>
              <th scope="col">Address Staking Contract</th>
            </tr>
          </thead>
          <tbody>
            {listStaking}
          </tbody>
        </table>

        <table className="table table-borderless text-muted text-center">
          <thead>
            <tr>
              <th scope="col">Staking Balance</th>
              <th scope="col">Reward Balance</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{window.web3.utils.fromWei(this.props.stakingBalance, 'Ether')} mRena</td>
              <td>{window.web3.utils.fromWei(this.props.daiTokenBalance, 'Ether')} mDAI</td>
            </tr>
          </tbody>
        </table>

        <div className="card mb-4">
          <div className="card-body">
            <form className="mb-3">
              <div>
                <label className="float-left"><b>Create Staking Contract</b></label>
              </div>
              <div className="input-group mb-4">
                <input
                  type="text"
                  name="rewardAddress"
                  className="form-control form-control-lg"
                  placeholder="reward token address"
                  onChange={this.handleChange}
                  required />
              </div>
              <div className="input-group mb-4">
                <input
                  type="text"
                  name="stakingAddress"
                  className="form-control form-control-lg"
                  placeholder="stake token address"
                  onChange={this.handleChange}
                  required />
              </div>
              <div className="input-group mb-4">
                <input
                  type="text"
                  name="APY"
                  className="form-control form-control-lg"
                  placeholder="APY"
                  onChange={this.handleChange}
                  required />
              </div>
              <div className="input-group mb-4">
                <input
                  type="text"
                  name="limited"
                  className="form-control form-control-lg"
                  placeholder="limited"
                  onChange={this.handleChange}
                  required />
              </div>
              <input type="submit" className="btn btn-primary btn-block btn-lg" value="Create" onClick={this.handleSubmit}/>
            </form>
          </div>
        </div>

      </div>
    );
  }
}

export default Main;