import React from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Redirect
} from 'react-router-dom';
import { Navigate } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import App from './App';
import Staking from './Staking';

export default function AppRouter() {
    let routes = null;
    routes = (
        <Routes>
          <Route exact path="/" component={App}>
            <Route exact path="/staking" component={Staking}/>
          </Route>
        </Routes>
    )
    return (
        <div>
          <Container className="p-5" style={{width: '100%'}}>
            <Router>
              {routes}
            </Router>
          </Container>
        </div>
    );
}

