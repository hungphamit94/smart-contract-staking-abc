import React, { Component } from 'react'
import Web3 from 'web3'
import DaiToken from '../abis/DaiToken.json'
import RenaToken from '../abis/RenaToken.json'
import Factory from '../abis/Factory.json'
import Navbar from './NavBar'
import Main from './Main'
import './App.css'
import Staking from './Staking'

class App extends Component {

  async componentWillMount() {
    await this.loadWeb3()
    await this.loadBlockchainData()
  }

  async loadBlockchainData() {
    const web3 = window.web3

    const accounts = await web3.eth.getAccounts()
    this.setState({ account: accounts[0] })

    const networkId = await web3.eth.net.getId()

    // Load DaiToken
    const daiTokenData = DaiToken.networks[networkId]
    if(daiTokenData) {
      const daiToken = new web3.eth.Contract(DaiToken.abi, daiTokenData.address)
      this.setState({ daiToken })
      let daiTokenBalance = await daiToken.methods.balanceOf(this.state.account).call()
      this.setState({ daiTokenBalance: daiTokenBalance.toString() })
    } else {
      window.alert('DaiToken contract not deployed to detected network.')
    }

    // Load DappToken
    const renaTokenData = RenaToken.networks[networkId]
    if(renaTokenData) {
      const renaToken = new web3.eth.Contract(RenaToken.abi, renaTokenData.address)
      this.setState({ renaToken })
      let renaTokenBalance = await renaToken.methods.balanceOf(this.state.account).call()
      this.setState({ renaTokenBalance: renaTokenBalance.toString() })
    } else {
      window.alert('DappToken contract not deployed to detected network.')
    }

    // Load Factory
    const factoryData = Factory.networks[networkId]
    if(factoryData) {
      const factory = new web3.eth.Contract(Factory.abi, factoryData.address)
      this.setState({ factory })
    } else {
      window.alert('TokenFarm contract not deployed to detected network.')
    }

    this.setState({ loading: false })
  }

  async loadWeb3() {
    if (window.ethereum) {
      window.web3 = new Web3(window.ethereum)
      await window.ethereum.enable()
    }
    else if (window.web3) {
      window.web3 = new Web3(window.web3.currentProvider)
    }
    else {
      window.alert('Non-Ethereum browser detected. You should consider trying MetaMask!')
    }
  }

  stakeTokens = (amount) => {
    this.setState({ loading: true })
    console.log('gia tri cua amount:'+amount)
    this.state.factory.methods.renaImplementStaking(0, amount).send({ from: this.state.account }).on('transactionHash', (hash) => {
        console.log(hash);
    })
    // this.state.daiToken.methods.approve(this.state.tokenFarm._address, amount).send({ from: this.state.account }).on('transactionHash', (hash) => {
    //   this.state.tokenFarm.methods.stakeTokens(amount).send({ from: this.state.account }).on('transactionHash', (hash) => {
    //     this.setState({ loading: false })
    //   })
    // })
  }

  createStakingContract = (rewardAddress, stakingAddress, apy, limited) => {
    this.state.factory.methods.createRenaStakingRewardContract(rewardAddress, stakingAddress, apy, limited).send({from: this.state.account}).on('transactionHash', (hash) => {
      console.log(hash);
    })
  }

  unstakeTokens = (amount) => {
    this.setState({ loading: true })
    // this.state.tokenFarm.methods.unstakeTokens().send({ from: this.state.account }).on('transactionHash', (hash) => {
    //   this.setState({ loading: false })
    // })
  }

  constructor(props) {
    super(props)
    this.state = {
      account: '0x0',
      daiToken: {},
      renaToken: {},
      factory: {},
      daiTokenBalance: '0',
      renaTokenBalance: '0',
      stakingBalance: '0',
      loading: true
    }
  }

  render() {
    let content
    if(this.state.loading) {
      content = <p id="loader" className="text-center">Loading...</p>
    } else {
      content = <Staking
        daiTokenBalance={this.state.daiTokenBalance}
        renaTokenBalance={this.state.renaTokenBalance}
        stakingBalance={this.state.stakingBalance}
        factory={this.state.factory}
        stakeTokens={this.stakeTokens}
        unstakeTokens={this.unstakeTokens}
        createStakingContract={this.createStakingContract}
        getRenaStakingRewards={this.getRenaStakingRewards}
      />
    }

    return (
      <div>
        <Navbar account={this.state.account} />
        <div className="container-fluid mt-5">
          <div className="row">
            <main role="main" className="col-lg-12 ml-auto mr-auto" style={{ maxWidth: '600px' }}>
              <div className="content mr-auto ml-auto">
                <a
                  href="http://www.dappuniversity.com/bootcamp"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                </a>

                {content}

              </div>
            </main>
          </div>
        </div>
      </div>
    );
  }
}

export default App;